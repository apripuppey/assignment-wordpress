<?php
/*
Template Name: Client Form
*/
get_header();
?>
<section>
    <div class="container">
        <h1 class="text-center"> Client Form </h1>
        <hr/>
            <?php echo do_shortcode('[contact-form-7 id="16" title="Client"]'); ?>
        
    </div>
</section>

<?php 
get_footer();
?>

