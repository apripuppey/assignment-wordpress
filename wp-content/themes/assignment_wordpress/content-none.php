<article class="no-results">
                
                <header class="entry-header">
                    <h1 class="page-title"><?php esc_html_e( 'Nothing Found', 'assignment_wordpress' ); ?></h1>
                </header><!--entry-header -->
            
                <div class="entry-content">
                    <p><?php esc_html_e( 'It looks like nothing was found at this location.', 'assignment_wordpress' ); ?></p>
                </div><!--entry-content -->
            
            </article><!--no-results -->