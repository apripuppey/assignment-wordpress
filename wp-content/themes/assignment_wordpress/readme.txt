=== Theme Name ===
Requires at least: 5.0
Tested up to: 5.2
Requires PHP: 5.6
License: GPLv2 or later
License URI: <https://www.gnu.org/licenses/gpl-2.0.html>

Custom theme for Assignment by Apriadi Nur Kurniawan

== Description ==
Custom theme for Assignment by Apriadi Nur Kurniawan

== Changelog ==

= 1.0 =
* First test

== Resources ==
* normalize.css <https://necolas.github.io/normalize.css/>, (C) 2012-2016 Nicolas Gallagher and Jonathan Neal, [MIT](<https://opensource.org/licenses/MIT>