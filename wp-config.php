<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'hourglass' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'A3)B;481O87VNBko=`x?>xiu{0K(JOU{A(INn9Abup6Tbx^@fs5^Py!zL^z<]9kc' );
define( 'SECURE_AUTH_KEY',  'L|x6H>lZ%Mwy.m|sB>xi|ul5GFUL.~z jTFs!(5C}>9/FG-^fvX$BZX3Yny]s0Ah' );
define( 'LOGGED_IN_KEY',    'UnO;Hu]}Txv12D+*iIPQ([#:dwYpq,zV~FUV7kY_N?FxfxO|N_{|PF*6!|i#X]>^' );
define( 'NONCE_KEY',        '{@fE}w^n8GnJI!pm84mwx%~ E(QHT,8g>[y*0]f^*I}jv-r8@/CfsM0kwO6*}t&h' );
define( 'AUTH_SALT',        '5r4b8L4/1}CNLznzd^(=t3&A61qHOPX[~J`z5? (>3sRnCsc}aOK,?`??Z2j!aRn' );
define( 'SECURE_AUTH_SALT', ')4EC0k>i|8{~/(PNHi_Da+kvbK<u>Ay[dc}rH/)S@D?a^nNP#gM22CNV>%M[f1fB' );
define( 'LOGGED_IN_SALT',   'bMcw[5`Qm9_FXT5+P>R_-%xv3`L@|fZPuD[5A:!6.`=5t(L]jXkGUnn5S8bHoJ0{' );
define( 'NONCE_SALT',       'Iw;;Z}CU4T u<6i.kY.6P%G fup_Kmr_6aVkg{;Xlm3tJnd#8l[Gn;{}9w; M}Gs' );

/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wphg_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Add any custom values between this line and the "stop editing" line. */



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
